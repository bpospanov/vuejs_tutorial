import { ref } from '@vue/runtime-core'

const getPosts = () => {
    const posts = ref([])
    const error = ref(null)

    const load = async () => {      
      try {
        let res = await fetch('http://localhost:3000/posts')
        if (!res.ok) {
          throw Error('url not avaliable')
        }
        posts.value = await res.json()
      } 
      catch (err) {
        error.value = err.message
      }
    }

    return { posts, error, load }
}

export default getPosts