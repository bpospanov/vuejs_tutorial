import { ref } from '@vue/runtime-core'

const getPost = (id) => {
    const post = ref(null)
    const error = ref(null)

    const load = async () => {
      try {
        // simulate delay
        await new Promise(resolve => {
            setTimeout(resolve, 500)
        })

        let res = await fetch('http://localhost:3000/posts/' + id)
        if (!res.ok) {
          throw Error('that post does not exist')
        }
        post.value = await res.json()
      } 
      catch (err) {
        error.value = err.message
      }
    }

    return { post, error, load }
}

export default getPost